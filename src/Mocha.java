
public class Mocha extends CondimentDecorator {
	private Beverage b;
	
	public Mocha(Beverage b){
		this.b = b;
	}
	
	@Override
	public String getDescription() {
		return this.b.getDescription() + ", Mocha";
	}

	@Override
	public double cost() {
		return this.b.cost() + 0.30;
	}

}
