
public class DecoratorPatternSample {

	public static void main(String[] args) {
		Beverage beverage1 = new Mocha(new Mocha(new Espresso()));
		System.out.println(beverage1.getDescription());
		System.out.println("Cost = " + beverage1.cost());
	}

}
