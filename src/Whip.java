
public class Whip extends CondimentDecorator {
	private Beverage b;
	
	public Whip(Beverage b){
		this.b = b;
	}
	
	@Override
	public String getDescription() {
		return this.b.getDescription() + ", Whip";
	}

	@Override
	public double cost() {
		return this.b.cost() + 0.1;
	}

}
